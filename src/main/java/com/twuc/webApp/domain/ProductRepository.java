package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    public List<Product> getProductsByProductLineTextDescriptionContains(String text);
    public List<Product> getProductsByQuantityInStockIsBetween(Short start, Short end);
    public List<Product> getProductsByQuantityInStockIsBetweenOrderByProductCode(Short start, Short end);
    public List<Product> findTop3ByQuantityInStockIsBetweenOrderByProductCode(Short start, Short end);
    public List<Product> getProductsByProductLineTextDescriptionLike(String text);
    public Page<Product> findAllByQuantityInStockBetweenOrderByProductCode(Short start, Short end, Pageable pageable);
//    public Page<Product> findAll(PageRequest.of())
    // --end-->
}
